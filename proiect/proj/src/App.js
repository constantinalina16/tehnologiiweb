import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';


import { Map, InfoWindow, Marker } from 'google-maps-react';
import { GoogleApiWrapper } from 'google-maps-react';

import {Add} from './Add';



export class App extends Component {
  
  constructor(props){
    super(props);
    this.state.addItem = [];
    
  }
  

  state = {
    showingInfoWindow: false, //Hides or the shows the infoWindow
    activeMarker: {}, //Shows the active marker upon click
    selectedPlace: {}, //Shows the infoWindow to the selected place upon a marker
    
  };

  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });

  onClose = props => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  };

  // constructor(props){
  //   super(props)
  //   this.state = {};
  //   this.state.clients = [];
  //   this.state.address=[];
  //   this.state.order=[];
  //   this.state.prod=[];

  // }

  onItemAdded = (client) => {
    this.state.clients.push(client);
    let clients = this.state.clients;
    this.setState({
      clients: clients
    })
    console.log(this.state.clients)
  }
  
  

  render() {
var style = {
  width: '60%',
  height: '60%',
  margin: '-385px 550px',
  float:'right',
  display:'inline'
}
    return (
      <div className="App">
        <h1 id="title">RESTaurant Manager</h1>
        <p>ORDER YOUR FAVOURITE FOOD</p>
        <div class="inputContainer">
        <Add></Add></div>
        <div class="mapContainer">
        <Map style="margin-top:20px;"
        google={this.props.google}
        zoom={12}
        style={style}
        initialCenter={{
       lat: 44.4267674, lng: 26.102538390000063
        }}>
         
        <Marker
          onClick={this.onMarkerClick}
          title={'The marker`s title will appear as a tooltip.'}
          name={'Ristorante Pizzeria La Terrazza'}
          position={{lat: 44.433058, lng: 26.069983}} />
        <Marker
          onClick={this.onMarkerClick}
          name={'La Samuelle'}
          position={{lat: 44.466290, lng: 26.085172}} />  
        <Marker />
        <Marker
          onClick={this.onMarkerClick}
          name={'Restaurant Pescăruș'}
          position={{lat: 44.472836, lng: 26.085715}} />    
        <Marker />
        <Marker
          onClick={this.onMarkerClick}
          name={'La Mahala'}
          position={{lat: 444.430910, lng: 26.101664}} />     
        <Marker />
        <Marker
          onClick={this.onMarkerClick}
          name={'Stadio'}
          position={{lat: 44.437487, lng: 26.099179}} />       
        <Marker />
        <Marker
          onClick={this.onMarkerClick}
          name={'Restaurant & Terasa Trei Camile'}
          position={{lat: 44.402353, lng: 26.092173}} />         
        <Marker />
        <InfoWindow
          marker={this.state.activeMarker}
          visible={this.state.showingInfoWindow}
          onClose={this.onClose}
        >
          <div>
             <h4>{this.state.activeMarker.name}</h4>
          </div>
        </InfoWindow>
      </Map>
      </div>
      </div>
    );
  }
}

//export default App;
export default GoogleApiWrapper({
  apiKey: ("AIzaSyAyvpz_Y5EP-Riadg2eYEHBJu86VK5agmE")
})(App)
