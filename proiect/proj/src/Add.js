import React,{Component} from 'react';
import axios from 'axios';


export class Add extends React.Component{
    constructor(props){
        super(props);
        this.state={
            firstName:'',
            lastName:'',
            mail:'',
            phone:'',
            street:'',
            streetNumber:0,
            building:'',
            apartment:0,
            city:'',
            date:'',
            payment:'card',
            orderState:'',
            name:'',
            category:''
        };
    }
    
    clearFields=()=>{
        this.setState({
            firstName:"",
            lastName:"",
            mail:"",
            phone:"",
            street:"",
            streetNumber:0,
            building:"",
            apartment:0,
            city:"",
            date:"",
            payment:"",
            orderState:"",
            name:"",
            category:""
        })
    }
    
    handleChangeFirstName=e=>{
        this.setState({
            firstName:e.target.value
        });
    }
    
    handleChangeLastName=(e)=>{
        this.setState({
            lastName:e.target.value
        });
    }
    
    handleChangeMail=(e)=>{
        this.setState({
            mail:e.target.value
        });
    }
    
    handleChangePhone=(e)=>{
        this.setState({
            phone:e.target.value
        });
    }
    
    /*Adresa*/
    
    handleChangeStreet=(e)=>{
        this.setState({
            street:e.target.value
        });
    }
    
    handleChangeStreetNumber=(e)=>{
        this.setState({
            streetNumber:e.target.value
        });
    }
    
    handleChangeBuilding=(e)=>{
        this.setState({
            building:e.target.value
        });
    }
    
    handleChangeApartment=(e)=>{
        this.setState({
            apartment:e.target.value
        });
    }
    
    handleChangeCity=(e)=>{
        this.setState({
            city:e.target.value
        });
    }
    
    onItemAdded = () => {
        var date=new Date();
      let client = {
          firstName: this.state.firstName,
          lastName: this.state.lastName,
          mail: this.state.mail,
          phone:this.state.phone,
          street:this.state.street,
            streetNumber:this.state.streetNumber,
            building:this.state.building,
            apartment:this.state.apartment,
            city:this.state.city,
          date: date.getDate(),
          payment: this.state.payment,
          orderState:this.state.orderState,
          name: this.state.name,
          category: this.state.category,
          
      }
      this.props.handleAdd(client);
      this.clearFields()
    }
    
    /*Categorie*/
    
    handleChangeDate=(e)=>{
        this.setState({
            date:e.target.value
        });
    }
    
    handleChangePayment=(e)=>{
        this.setState({
            payment:e.target.value
        });
    }
    
    handleChangeOrderState=(e)=>{
        this.setState({
            orderState:e.target.value
        });
    }
    
    /*----------------produs-------------------------------*/
    
    handleChangeName=(e)=>{
        this.setState({
            name:e.target.value
        });
    }
    
    handleChangeCategory=(e)=>{
        this.setState({
            category:e.target.value
        });
    }
    
    handleSubmit = e => {
    e.preventDefault();

    // const userform = {userid: this.state.userid};
    // const fullnameForm = {fullname: this.state.fullname};
    // const usergroupForm = {usergroup: this.state.usergroup};
    // const emailidForm = {emailid: this.state.emailid};
    // const mobileForm = {mobile: this.state.mobile};
    // const titleForm = {title: this.state.title};
    console.log(this.state.firstName);
    const {fname,lname,mail,phone}=this.state;
    axios.post('/exitclients',
      { fname, lname, mail, phone })
      .then(res => {
        console.log(res);
        console.log(res.data);
      });
  }

    
    
    
    render(){
        return(
            <React.Fragment>
            
            <form onSubmit={this.handleSubmit.bind(this)}>
            
            <div class="input">
            <label>Nume: </label>
            <input type="text" name="this.state.lastName" onChange={this.handleChangeLastName} />
            </div>
            <br />
            
            <div class="input">
            <label>Prenume: </label>
            <input type="text"  name="this.state.firstName" onChange={this.handleChangeFirstName} />
            </div>
            <br />
            
            <div class="input">
            <label>Mail: </label>
            <input type="text" name="this.state.mail" onChange={this.handleChangeMail} />
            </div>
            <br />
            
            <div class="input">
            <label>Telefon: </label>
            <input type="text" name="this.state.phone" onChange={this.handleChangePhone} />
            </div>
            
            <br />
             <label>Street: </label>
            <input type="text" value={this.state.street} onChange={this.handleChangeStreet} /><br />
            
            <label>Street Number: </label>
            <input type="text" value={this.state.streetNumber} onChange={this.handleChangeStreetNumber} />
            <br />
            
            <label>Building: </label>
            <input type="text"  value={this.state.building} onChange={this.handleChangeBuilding} />
            <br />
            
            <label>Apartment: </label>
            <input type="text" value={this.state.apartment} onChange={this.handleChangeApartment} />
            <br />
            
            <label>City: </label>
            <input type="text" placeholder="City" value={this.state.city} onChange={this.handleChangeCity} />
            <br />
     
            <label>Produs: </label>
            <input type="text"  onChange={this.handleChangeName} />
            <br />
            
            <label>Category: </label>
            <input type="text" value={this.state.category} onChange={this.handleChangeCategory} />
            <br />
            
            <label>Restaurant: </label>
            <input type="text" value={this.state.category} onChange={this.handleChangeCategory} />
            <br />
            
            <input type="submit" value="Salveaza Comanda" />
            </form>
            
            </React.Fragment>
            );
    }
}


