const express=require('express');

const app=express();

app.use('/',express.static('components'));

app.listen(8080);

const Sequelize= require('sequelize');

const sequelize=new Sequelize('ManagerRestaurant','root','',{
    dialect:"mysql",
    host:"localhost"
});

sequelize.authenticate()
.then(()=>{
    console.log("Te-ai conectat cu succes la baza de date!")
})
.catch(()=>{
    console.log("Nu te-ai putut conecta la baza de date!")
})

const Clients=sequelize.define('clients',{
    clientId:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement: true
    },
    firstName:{
        type:Sequelize.STRING,
        allowNull:false
    },
    lastName:{
        type:Sequelize.STRING,
        allowNull:false
    },
    /*address:{
        type:Sequelize.TEXT,
        allowNull:false
    },*/
    mail:{
        type:Sequelize.TEXT,
        allowNull:true
    },
    phone:{
        type:Sequelize.STRING,
        allowNull:false
    }
});

const Addresses=sequelize.define('addresses',{
    /*clientId:{
        type:Sequelize.UUID,
        allowNull:false
        },*/
    street:{
        type:Sequelize.STRING,
        allowNull:false
    },
    streetNumber:{
        type:Sequelize.INTEGER,
        allowNull:false
    },
    building:{
        type:Sequelize.STRING,
        allowNull:true
    },
    apartment:{
        type:Sequelize.INTEGER,
        allowNull:true
    },
    city:{
        type:Sequelize.STRING,
        allowNull:false
    }
});

const Orders=sequelize.define('orders',{
    orderId:{
        type:Sequelize.INTEGER,
        autoIncrement:true,
        primaryKey:true
        },
    /*clientId:{
        type:Sequelize.INTEGER,
        //autoIncrement:true
    },*/
    date:{
        type:Sequelize.DATEONLY,
        allowNull:false
    },
    payment:{
        type:Sequelize.TEXT,
        allowNull:false
    },
    orderState:{
        type:Sequelize.STRING,
        allowNull:false
    }
});

const Deliveries=sequelize.define('deliveries',{
    /*orderId:{
        type:Sequelize.UUID,
        primaryKey:true
    },*/
    //productId:Sequelize.INTEGER,
    deliveryId:{
        type:Sequelize.INTEGER,
        primaryKey:true,
        autoIncrement:true
    },
    price:{
        type:Sequelize.FLOAT,
        allowNull:false
    },
    quantity:{
        type:Sequelize.INTEGER,
        allowNull:false
    }
});

const Products=sequelize.define('products',{
    productId:{
        type:Sequelize.INTEGER,
        autoIncrement:true,
        primaryKey:true
    },
    name:{
        type:Sequelize.STRING,
        allowNull:false
    },
    category:{
        type:Sequelize.STRING,
        allowNull:false
    },
    details:{
        type:Sequelize.TEXT,
        allowNull:true
    },
    numeRestaurant:{
        type:Sequelize.STRING,
        allowNull:false
    }
});

//Associations
Addresses.belongsTo(Clients,{foreignKey:'clientId'});
//Clients.belongsTo(Addresses,{foreignKey:'clientId'});
Clients.hasMany(Orders,{foreignKey:'cliendId'});
Orders.hasMany(Products,{foreignKey:'orderId'});
Deliveries.hasMany(Orders,{foreignKey:'deliveryId'})

//sequelize.sync();

//Sincronizare
sequelize.sync({force:true})
.then(()=>{
    console.log("Tabelele au fost create cu succes!")
})
.catch((err)=>{
     console.log(err)
})

app.use(express.json());

app.post('/exitclients',(request,response)=>{
    Clients.create(request.body)
    .then((result)=>{
        response.status(201).json(result)
    })
    .catch((err)=>{
        response.status(500).send("Inregistrarea nu a putut fi facuta!")
    })
})

app.post('/orders',(request,response)=>{
    Orders.create(request.body)
    .then((result)=>{
        response.status(201).json(result)
    })
    .catch((err)=>{
        response.status(500).send("Inregistrarea nu a putut fi facuta!")
    })
})

app.post('/addresses',(request,response)=>{
     Addresses.create(request.body)
    .then((result)=>{
        response.status(201).json(result)
    })
    .catch((err)=>{
        response.status(500).send("Inregistrarea nu a putut fi facuta!")
    })
})

app.post('/products',(request,response)=>{
     Products.create(request.body)
    .then((result)=>{
        response.status(201).json(result)
    })
    .catch((err)=>{
        response.status(500).send("Inregistrarea nu a putut fi facuta!")
    })
})


app.post('/deliveries',(request,response)=>{
     Deliveries.create(request.body)
    .then((result)=>{
        response.status(201).json(result)
    })
    .catch((err)=>{
        response.status(500).send("Inregistrarea nu a putut fi facuta!")
    })
})

app.get('/orders', (request, response) => {
    Orders.findAll().then((results) => {
        response.status(200).json(results)
    })
})

app.get('/orders/:id', (request, response) => {
    Orders.findById(request.params.id).then((result) => {
        if(result) {
            response.status(200).json(result)
        } else {
            response.status(404).send('NU a fost gasit!')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('ERROR!')
    })
})

app.get('/clients', (request, response) => {
    Clients.findAll().then((results) => {
        response.status(200).json(results)
    })
})

app.get('/clients/:id', (request, response) => {
    Clients.findById(request.params.id).then((result) => {
        if(result) {
            response.status(200).json(result)
        } else {
            response.status(404).send('NU a fost gasit!')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('ERROR!')
    })
})


app.put('/updateState/:orderId',(request,response)=>{
    Orders.findById(request.params.orderId).then((order)=>{
        if(order){
            order.update(request.body).then((result)=>{
                response.status(201).json(result)
            }).catch((err)=>{
                console.log(err)
                response.status(500).send('ERROR!')
            })
        }else{
            response.status(404).send('NU a fost gasit!')
        }
    }).catch((err)=>{
        response.status(500).send('ERROR!')
    })
})

app.put('/updateProduct/:productId',(request,response)=>{
    Products.findById(request.params.productId).then((product)=>{
        if(product){
            product.update(request.body).then((result)=>{
                response.status(201).json(result)
            }).catch((err)=>{
                console.log(err)
                response.status(500).send('ERROR!')
            })
        }else{
            response.status(404).send('NU a fost gasit!')
        }
    }).catch((err)=>{
        response.status(500).send('ERROR!')
    })
})

app.delete('/orders/:orderId', (request, response) => {
    Orders.findById(request.params.orderId).then((order) => {
        if(order) {
            order.destroy().then((result) => {
                response.status(204).send()
            }).catch((err) => {
                console.log(err)
                response.status(500).send('ERROR!')
            })
        } else {
            response.status(404).send('Nu a fost gasit!')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('ERROR!')
    })
})

app.delete('/products/:productId', (request, response) => {
    Products.findById(request.params.productId).then((product) => {
        if(product) {
            product.destroy().then((result) => {
                response.status(204).send()
            }).catch((err) => {
                console.log(err)
                response.status(500).send('ERROR!')
            })
        } else {
            response.status(404).send('Nu a fost gasit!')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('ERROR!')
    })
})

